resource "aws_instance" "appServer" {
  ami           = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.medium"
	tags = {
   		Name  = "jlinn test"
   		Owner = "jlinn@hashicorp.com"
   		TTL   = 24
	}
}



